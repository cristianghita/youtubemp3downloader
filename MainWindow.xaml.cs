﻿using MediaToolkit.Model;
using System.IO;
using System.Windows;
using VideoLibrary;
using YtMp3Dl.Utils;

namespace YtMp3Dl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = VmService.MainWindowVm;
        }

        public void Download(object parameter = null)
        {
            var source = @"C:\Users\Cristian.Ghita\source\repos\YoutubeMp3Downloader\Other";
            var youtube = YouTube.Default;
            var vid = youtube.GetVideo(@"https://www.youtube.com/watch?v=dlO8aANekSM");
            File.WriteAllBytes(source + vid.FullName, vid.GetBytes());

            var inputFile = new MediaFile { Filename = source + vid.FullName };
            var outputFile = new MediaFile { Filename = $"{source + vid.FullName}.mp3" };

            using (var engine = new MediaToolkit.Engine())
            {
                engine.GetMetadata(inputFile);
                engine.Convert(inputFile, outputFile);
            }
        }
    }
}
