﻿using YtMp3Dl.ViewModel;

namespace YtMp3Dl.Utils
{
    public static class VmService
    {
        private static readonly MainWindowVm _mainWindowsVm;
        public static MainWindowVm MainWindowVm = _mainWindowsVm ?? (_mainWindowsVm = new MainWindowVm());
    }
}
