﻿namespace YtMp3Dl.ViewModel
{
    public class SongVm : BaseVm
    {
        private string _artistTitle;
        public string ArtistTitle
        {
            get => _artistTitle;
            set
            {
                if (_artistTitle == value) return;

                _artistTitle = value;
                OnPropertyChanged(nameof(ArtistTitle));
            }
        }

        private int _progress;
        public int Progress
        {
            get => _progress;
            set
            {
                if (_progress == value) return;

                _progress = value;
                OnPropertyChanged(nameof(Progress));
            }
        }
    }
}
