﻿using MediaToolkit.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VideoLibrary;
using YoutubeExplode;
using YoutubeExplode.Models.MediaStreams;
using YtMp3Dl.Utils;

namespace YtMp3Dl.ViewModel
{
    public class MainWindowVm : BaseVm
    {
        #region Bound Properties - Individual

        private string _link;
        public string Link
        {
            get => _link;
            set
            {
                if (_link == value) return;

                _link = value;
                OnPropertyChanged(nameof(Link));
            }
        }

        private string _artist;
        public string Artist
        {
            get => _artist;
            set
            {
                if (_artist == value) return;

                _artist = value;
                OnPropertyChanged(nameof(Artist));
            }
        }

        private string _title;
        public string Title
        {
            get => _title;
            set
            {
                if (_title == value) return;

                _title = value;
                OnPropertyChanged(nameof(Title));
            }
        }

        private string _targetFolderPath = @"C:\Users\Cristian.Ghita\source\repos\YtMp3Dl\Other";
        public string TargetFolderPath
        {
            get => _targetFolderPath;
            set
            {
                if (_targetFolderPath == value) return;

                _targetFolderPath = value;
                OnPropertyChanged(nameof(TargetFolderPath));
            }
        }

        private string _status;
        public string Status
        {
            get => _status;
            set
            {
                if (_status == value) return;

                _status = value;
                OnPropertyChanged(nameof(Status));
            }
        }

        private ObservableCollection<SongVm> _songs = new ObservableCollection<SongVm>();
        public ObservableCollection<SongVm> Songs
        {
            get => _songs;
            set
            {
                if (_songs == value) return;

                _songs = value;
                OnPropertyChanged(nameof(Songs));
            }
        }

        #endregion

        #region Methods

        public void Download(object parameter = null)
        {
            LoadTitle();
            FormatTitleArtist();
        }

        private void FormatTitleArtist()
        {
            if (!Title.Contains("-")) return;

            Artist = Title.Substring(0, Title.IndexOf('-')).Trim();
            Title = Title.Substring(Title.IndexOf('-') + 1).Trim();

            if (Title.Contains("ft.")) MoveFeaturing("ft.");
            if (Title.Contains("feat.")) MoveFeaturing("feat.");
        }

        private void MoveFeaturing(string keyword)
        {
            var indexOfKeyword = Title.IndexOf(keyword);
            var bracket = Title.IndexOf(')');
            var math = Title.Length - Title.IndexOf(keyword);
            var x = Title.Substring(Title.IndexOf(keyword),
                Title.Substring(Title.IndexOf(keyword)).Contains(")") 
                    ? (Title.IndexOf(')') > Title.IndexOf(keyword) 
                        ? Title.IndexOf(')') - Title.IndexOf(keyword)
                        : Title.Substring(Title.IndexOf(keyword)).IndexOf(')'))
                    : Title.Length - Title.IndexOf(keyword));
            Artist += $" {x}";
            Title = Title.Replace(x, string.Empty);

            if (Title.Contains("()")) Title = Title.Replace("()", string.Empty);

            Title = Title.Trim();
            Artist = Artist.Trim();
        }

        private void LoadTitle()
        {
            var id = YoutubeClient.ParseVideoId(Link);

            var client = new YoutubeClient();
            var video = Task.Run(async () => await client.GetVideoAsync(id)).Result;

            var title = video.Title;

            Title = title;
        }

        private string DownloadYoutubeMp3(string link, string artist, string title)
        {
            var client = new YoutubeClient();
            var id = YoutubeClient.ParseVideoId(link);

            //var author = video.Author;
            //var duration = video.Duration;

            var streamInfoSet = Task.Run(async () => await client.GetVideoMediaStreamInfosAsync(id)).Result;
            var streamInfo = streamInfoSet.Audio.WithHighestBitrate();

            var ext = streamInfo.Container.GetFileExtension();

            var webmPath = $@"{TargetFolderPath}\{artist} - {title}.{ext}";
            Task.Run(async () => await client.DownloadMediaStreamAsync(streamInfo, webmPath));

            updateProgress(title);
            return webmPath;
        }

        private void ConvertWebmToMp3(string pathToWebm, string pathToTargetFile, string title)
        {
            cs_ffmpeg_mp3_converter.FFMpeg.Convert2Mp3(pathToWebm, pathToTargetFile);
            updateProgress(title);
        }

        private void SetFileTags(string pathToTargetFile, string link, string artist, string title, string pathToWebm)
        {
            while (!File.Exists(pathToTargetFile)) { Thread.Sleep(200); if(!File.Exists(pathToTargetFile)) ConvertWebmToMp3(pathToWebm, pathToTargetFile, title); }
            var id = YoutubeClient.ParseVideoId(link);
            TagLib.File tlFile = TagLib.File.Create(pathToTargetFile);
            tlFile.Tag.Artists = new string[] { artist };
            tlFile.Tag.AlbumArtists = new string[] { artist };
            tlFile.Tag.Performers = new string[] { artist };

            tlFile.Tag.Title = title;
            
            tlFile.Save();
            updateProgress(title);
        }

        private void SetFileImg(string pathToTargetFile, string link, string title)
        {
            var id = YoutubeClient.ParseVideoId(link);
            string videoImgUrl = $"https://img.youtube.com/vi/{id}/mqdefault.jpg";
            byte[] imageBytes;
            using (var webClient = new WebClient())
            {
                imageBytes = webClient.DownloadData(videoImgUrl);
            }
            TagLib.Id3v2.AttachedPictureFrame cover = new TagLib.Id3v2.AttachedPictureFrame
            {
                Type = TagLib.PictureType.FrontCover,
                Description = "Cover",
                MimeType = System.Net.Mime.MediaTypeNames.Image.Jpeg,
                Data = imageBytes,
                TextEncoding = TagLib.StringType.UTF16
            };
            TagLib.File tlFile = TagLib.File.Create(pathToTargetFile);
            tlFile.Tag.Pictures = new TagLib.IPicture[] { cover };
            tlFile.Save();
            updateProgress(title);
        }

        public void Save(object parameter = null)
        {
            new Task(DoTheWholeThing).Start();
            //Task.Run(() => DoTheWholeThing());
        }

        private void DoTheWholeThing()
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
               Songs.Add(new SongVm
               {
                   ArtistTitle = $"{Artist} - {Title}"
               });
            });
            
            Status = $"Started {Artist} - {Title}";
            var _persistLink = Link;
            var _persistArtist = Artist;
            var _persistTitle = Title;
            var webmPath = DownloadYoutubeMp3(_persistLink, _persistArtist, _persistTitle);
            var pathToTargetFile = $@"{TargetFolderPath}\{_persistArtist} - {_persistTitle}.mp3";
            ConvertWebmToMp3(webmPath, pathToTargetFile, _persistTitle);
            SetFileTags(pathToTargetFile, _persistLink, _persistArtist, _persistTitle, webmPath);
            SetFileImg(pathToTargetFile, _persistLink, _persistTitle);
            Clean(webmPath, _persistTitle);
        }

        private void updateProgress(string title)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                Songs.First(x => x.ArtistTitle.Contains(title)).Progress += 20;
            });
        }

        private void Clean(string webmPath, string title)
        {
            File.Delete(webmPath);
            updateProgress(title);
        }

        public void Browse(object parameter = null)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                TargetFolderPath = folderBrowserDialog.SelectedPath;
        }

        #endregion

        #region Commands

        private RelayCommand _downloadCommand;
        public RelayCommand DownnloadCommand => _downloadCommand ?? (_downloadCommand = new RelayCommand(Download));

        private RelayCommand _saveCommand;
        public RelayCommand SaveCommand => _saveCommand ?? (_saveCommand = new RelayCommand(Save));

        private RelayCommand _browseCommand;
        public RelayCommand BrowseCommand => _browseCommand ?? (_browseCommand = new RelayCommand(Browse));

        #endregion
    }
}
